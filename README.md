**声明：**

为了加快访问速度，提升各位新老股东阅读体验，我对JavaPub做了一些完善，文末加p哥微信，看最少必要面试题。



**【学习+实战+面试】一份涵盖大部分程序员需要掌握的核心知识，从基础到组件到项目源码。**

spider



`善于使用 ctrl + f`



| 收录     | 地址                                       |
| -------- | ------------------------------------------ |
| 谷歌收录 | https://www.google.com.hk/search?q=javapub |
| 百度收录 | https://www.baidu.com/s?wd=javapub         |
| 必应收录 | https://www4.bing.com/search?q=javapub     |
| 360收录  | https://www.so.com/s?q=javapub             |
| 搜狗收录 | https://www.sogou.com/web?query=javapub    |




[toc]

# JavaPub

## 声明



>  SpringBoot最新版常用案例整合，持续更新中 https://github.com/Rodert/SpringBoot-javapub
>
>  国内访问：https://gitee.com/rodert/SpringBoot-javapub



<p align="center">
  <a href="#公众号"><img src="https://img.shields.io/badge/关注公众号-JavaPub-blue.svg" alt="公众号"></a>
  <a href="https://juejin.im/user/5c00ce5a5188256d9832cb5f"><img src="https://img.shields.io/badge/关注-掘金-lightgrey.svg" alt="掘金"></a>
  <a href="https://www.zhihu.com/people/zhui-ma-7-49"><img src="https://img.shields.io/badge/关注-知乎-critical.svg" alt="知乎"></a>
  <a href="https://segmentfault.com/blog/JavaPub"><img src="https://img.shields.io/badge/推荐阅读-思否-brightgreen.svg" alt="思否"></a>
  <a href="">	
</p>





## 文章列表

###  📚最少必要面试题  ###

- [Java基础](https://mp.weixin.qq.com/s?__biz=MzUzNDUyOTY0Nw==&mid=2247485967&idx=1&sn=d388e0b9c58e1c8344acd9f5f2d309bc&chksm=fa92190fcde590193f16dac886dc3f900cbd55c1c140476b7123b20bdc9f72b6280892449aeb&token=1494980440&lang=zh_CN#rd)
- [Java并发入门](https://mp.weixin.qq.com/s?__biz=MzUzNDUyOTY0Nw==&mid=2247486003&idx=1&sn=1c537d81d55cc4bdeeb98e159f77e3b3&chksm=fa921933cde590257baeb2e404fc24ffd2c91617459d39e115838d7f71c7e571cd1750d835c4&token=1494980440&lang=zh_CN#rd)
- [MySQL](https://mp.weixin.qq.com/s?__biz=MzUzNDUyOTY0Nw==&mid=2247486012&idx=1&sn=8b2ef30d6a43170a961fcb9cb219a3d5&chksm=fa92193ccde5902a978956cfe25c487322091fcb5bd8838a35aad2834d4b92e743a9bfb87a2d&token=1494980440&lang=zh_CN#rd)
- [ElasticSearch](https://mp.weixin.qq.com/s?__biz=MzUzNDUyOTY0Nw==&mid=2247486210&idx=1&sn=195b65bb00192069b497c1322ea9322c&chksm=fa921802cde59114a659967b893e85f201926473673f30ffc986d22193c4e174272668744e07&token=1494980440&lang=zh_CN#rd)


###  📖知识点总结  ###

> 下面是原创**PDF**干货版，持续更新中。


- [51页的MyBatis](https://mp.weixin.qq.com/s/op9ADw_6U5MhbcUlkFtOUQ)

- [14页的zookeeper总结](https://mp.weixin.qq.com/s/HfZ3nmTqCYHRhUkoSMEZAg)

...


###  ☕️Java基础  ###

#### 锁 ####

- [volatile关键字的作用](https://mp.weixin.qq.com/s/nNHr4bTLLVht_PiLWr0kQA)

#### jdk8 ####

- [原来ThreadLocal的Lambda构造方式这么简单](https://mp.weixin.qq.com/s/bXx5jBpghfR83uo6zrilrQ)

###  📝数据结构与算法  ###

1. [冒泡排序就是这么容易](https://mp.weixin.qq.com/s/ptxxmbfqndZeFHAfm3hdkw)
2. [选择排序就是这么容易](https://mp.weixin.qq.com/s/EirRqU1F6Zv0JLIfjJyC6w)
3. [插入排序就是这么容易](https://mp.weixin.qq.com/s/cCv5s7b_ACF3mZo6wSfOIA)
4. [希尔排序就是这么容易](https://mp.weixin.qq.com/s/x5nAtijd_zbOI4V2Z1xM1Q)
5. [归并排序就是这么容易](https://mp.weixin.qq.com/s/VM9R4Y3uvFcmRuvmoWxLdw)
6. [快速排序就是这么容易](https://mp.weixin.qq.com/s/DyUR6khAHdcKzHg7owHtlQ)
7. [堆排序就是这么容易](https://mp.weixin.qq.com/s/yxYz2kbqu-W7aeFvFv05BQ)
8. [计数排序就是这么容易](https://mp.weixin.qq.com/s/7lphoHUgfDu0Cb1cO8ExKA)
9. [桶排序就是这么容易](https://mp.weixin.qq.com/s/10GFjOZ2VgA06hWe_wkmwQ)
10. [基数排序就是这么容易](https://mp.weixin.qq.com/s/z4jzP2cew8lMWRgD7H-6JA)
- [rodert熬夜写了一份BloomFilter总结](https://mp.weixin.qq.com/s/6S9eu_eLBDxeetygulmHBw)
- [哈希算法篇 - 布隆过滤器](https://mp.weixin.qq.com/s/bj5s0jBSu4P9WbSN1sTFPA)
- [B树和B+树的区别](https://mp.weixin.qq.com/s/RWkc2lNarKnn8Dc0HrP58g)




###  📣Mybatis  ###


- [rodert熬夜写了一份Mybatis总结](https://mp.weixin.qq.com/s/op9ADw_6U5MhbcUlkFtOUQ)
- [MyBatis SQL 批量更新（代码+案例）](https://mp.weixin.qq.com/s/VYvciG9iOz1VwCPBzoQdhw)
- 



###  🔬搜索  ###
####  Lucene  ####

- [Lucene就是这么容易](https://mp.weixin.qq.com/s/AhirDnW4ul5xR1bBtKqtFQ)

####  Elasticsearch  ####

- [Springboot2.x整合ElasticSearch7.x实战目录](https://mp.weixin.qq.com/s/nSWEIfbpRf-4txJqRz60gQ)
- [Springboot2.x整合ElasticSearch7.x实战（一）](https://mp.weixin.qq.com/s/4azw3QycpqrprABNdo44Zg)
- [Springboot2.x整合ElasticSearch7.x实战（二）](https://mp.weixin.qq.com/s/R7bFN9pSA4XFVdZMfzYz_w)
- [Springboot2.x整合ElasticSearch7.x实战（三）](https://mp.weixin.qq.com/s/6qSSWGnxmiLjnaM-m1_wUQ)



###  🎩Spring  ###

Spring 学习路线图：
https://img-blog.csdnimg.cn/20201230220120483.png

- [一篇告诉你什么是Spring](https://mp.weixin.qq.com/s/5nj-AAekF8j5KL6J67UZKA)
- [第一个Spring程序(代码篇)](https://mp.weixin.qq.com/s/_T9g67UUI3ao33Ug1PNo_Q)
- [手把手整合SSM框架-附源码](https://mp.weixin.qq.com/s/qikPY9ymX4LTgLunrD8vUQ)

- [公司这套架构统一处理 try...catch 这么香，求求你不要再满屏写了，再发现扣绩效！(全局异常处理)](https://mp.weixin.qq.com/s/KjpZQX5-cHDWyiytkAQbFw)
- [CTO 说了，如果发现谁用 kill -9 关闭程序就开除](https://mp.weixin.qq.com/s/1Oyt8mn11wztY3zBWTEKkg)
- [spring的controller是单例还是多例？怎么保证并发的安全](https://mp.weixin.qq.com/s/z8OmcbgGI7GGBrbCqCNXbw)
- [真的！@Autowired和@Resource注解别再用错了！](https://mp.weixin.qq.com/s/6FycP6xVm5KQ9XoVn0KZ0Q)



#### Spring Boot ####

SpringBoot最新版常用案例整合，持续更新中 **https://github.com/Rodert/SpringBoot-javapub**

- [SpringBoot快速入门-附源码](https://mp.weixin.qq.com/s/jtnwb1pTrjXiFc9EvWwLnQ)
- [Springboot项目的接口防刷](https://mp.weixin.qq.com/s/ihqyJTievMCTBXlA6QgaBA)
- [SpringBoot 中的线程池，你真的会用么](https://mp.weixin.qq.com/s/kqFEKJ_PrlhqnrkCYBomYQ)
- [docker 打包 springboot 项目快速入门](https://mp.weixin.qq.com/s/kCdqHqPqiqXYGvsD267Adw)
- [自定义注解+AOP切面日志+源码](https://mp.weixin.qq.com/s/onYeB2EMRIsZS1j86X0ijA)



###  💞中间件  ###
####  zookeeper  ####
- [rodert熬夜写了一份zookeeper总结](https://mp.weixin.qq.com/s/HfZ3nmTqCYHRhUkoSMEZAg)

####  RocketMQ  ####
- [RocketMq 快速入门教程](https://mp.weixin.qq.com/s/kmUlX-5CtWVn0Qli1iyWng)




###  💍Redis  ###

- [rodert单排学习redis入门【黑铁】](https://mp.weixin.qq.com/s/lpYemCdb1KsE32UTTdxuxg)
- [rodert 单排学习 redis 进阶【青铜】](https://mp.weixin.qq.com/s/S2qZiJG-_HgW3ET9Sl0EAg)
- [rodert单排学习redis进阶【白银一】](https://mp.weixin.qq.com/s/hXkoUOJCl8zxDinS878Sqw)
- [rodert熬夜写了一份BloomFilter总结](https://mp.weixin.qq.com/s/6b5y8l9qIoD6VXdDZuIgBQ)
- [了解Redis过期策略及实现原理](https://mp.weixin.qq.com/s/GFtaDZb68XcoVQn6F2U_0Q)
- [缓存：热点key重建优化](https://mp.weixin.qq.com/s/_AfOygfCd-pCJ1lh2p-q0A)
- [记一次redis线上问题](https://mp.weixin.qq.com/s/blf1a6cP-oJQ9FmnpU_ocw)
- [了解Redis过期策略及实现原理](https://mp.weixin.qq.com/s/GFtaDZb68XcoVQn6F2U_0Q)



###  📚sql  ###


- [求求你不要再用offset和limit了](https://mp.weixin.qq.com/s/I6MJRMNy4G2yj3BlXghyFA)
- [慢查询优化方案-SQL篇【JavaPub版】](https://mp.weixin.qq.com/s/2ZoMtoA4MQVxOun2HPbVfQ)
- [分表分库解决思路](https://mp.weixin.qq.com/s/jt5oyx3oyC2uORXx8bk4UA)
- [如果mysql磁盘满了，会发生什么？还真被我遇到了！](https://mp.weixin.qq.com/s/mucnXyAcyFHGmA47GT-uig)

###  📚设计模式  ###

- [优雅的替换if-else语句](https://mp.weixin.qq.com/s/nVl2wb1Pr2tChy6kYw51MQ)
- [单例模式 --- 生产环境怎么用](https://mp.weixin.qq.com/s/s1cwut9WvUSrMYw_6UK3sg)

###  🔒分布式  ###

- [分布式唯一ID解决方案-雪花算法](https://mp.weixin.qq.com/s/k-x6Wz7ibEIGbRWN55YpnQ)



###  🌋shell  ###

- [jar包shell启动脚本](https://mp.weixin.qq.com/s/j6BeXX9sYUop-AomkQY4-Q)


###  ⚡️常用工具  ###
#### Git ####

- [Git【入门】这一篇就够了](https://mp.weixin.qq.com/s/keQpwkwXbiUxsjQNxse2mw)
- [国内加速访问Github的办法，超级简单](https://mp.weixin.qq.com/s/Wx1Q-pFUi5SWHoGKAFd3pg)

#### shell ####

- [代替xshell的国产免费工具](https://mp.weixin.qq.com/s/IKgP9ZlHBj0yo1mnLtjVWg)

#### linux  ####

- [史上最全win10下Linux子系统的安装及优化方案](https://mp.weixin.qq.com/s/jq6oz7L07WZqLS8WPwzGyA)

###  🤖web实战  ###

github地址：https://github.com/Rodert/JavaPub-Web

- [SSM项目合集（公众号领取）]()
- [基于SSM图书馆管理系统](https://mp.weixin.qq.com/s/rydMsMCGZG5F-F-LpXS_0A)
- [私活利器 时薪翻一番，推荐几个SpringBoot项目，建议你改改](https://mp.weixin.qq.com/s/_98QkdxnDZcORMCGWb44VQ)
- [16K点赞 基于Spring + Vue的前后端分离管理系统ELAdmin，真香](https://mp.weixin.qq.com/s/xW6H7Zv06jQYPbFPzHmcsw)
- [Spring Boot + Security + MyBatis + Thymeleaf + Activiti 快速开发平台项目](https://mp.weixin.qq.com/s/2ER31ZqgakXM6HjHI7-ckg)
- [一款基于 Spring Boot 的现代化社区（论坛/问答/社交网络/博客）](https://mp.weixin.qq.com/s/iyaD_KL-QCmhkhdNgPcQlg)
- [决定做一个开源项目](https://mp.weixin.qq.com/s/EcstWlAPtGbE9enfUGrqpA)






###  🚀实战面试  ###
- [Java 同学注意！这不是演习](https://mp.weixin.qq.com/s/RfQNwMdSLhvzza9zggjjaQ)

####  20212021 Java面试题系列教程  ####

- [Java基础--2021面试题系列教程--大白话解读--JavaPUb版本](https://mp.weixin.qq.com/s/Ogxm1whPUyCnnuBmBWUK1A)
- [Java容器--2021面试题系列教程（附答案解析）--大白话解读--JavaPub版本](https://mp.weixin.qq.com/s/6zjw2coKzlkoKATNMJ43lQ)
- [Java反射--2021面试题系列教程--大白话解读--JavaPub版本](https://mp.weixin.qq.com/s/xk7eU4uhDcCJRZNbcweqLw)
- 

### 🚀源码 ###

#### guava ####

- [guava中Lists.newArrayListWithCapacity()的作用和实现原理](https://mp.weixin.qq.com/s/xwbx43fLL0I0CjCqz_jvhg)
- 



### 🌊杂文随笔 ###

- [拒绝网贷](https://mp.weixin.qq.com/s/Qfz3ZwIRkKeigH8RxT_cbQ)
- [我的炒股生涯（非广告）](https://mp.weixin.qq.com/s/GsdjlkdOYkQbAEV2w6o01A)
- [平安夜有话想对你说](https://mp.weixin.qq.com/s/dog6TAbLUIHpVazmnhxC5Q)
- [没有一个人可以瘦着过完春节](https://mp.weixin.qq.com/s/0GRESHMDI5r1pxj94E7cEA)
- [一个中科大差生的8年程序员工作总结](https://mp.weixin.qq.com/s/SnsH9klKQM96fd2vYZpw4Q)
- [国企程序员是一种怎样的体验](https://mp.weixin.qq.com/s/XJ4r-Xhkyu6fpoPbq2PW6g)
- [越努力，](https://mp.weixin.qq.com/s/YdeXrlSNSgqcALL-ZURYmg)
- [一个员工的离职成本到底有多高？](https://mp.weixin.qq.com/s/TCUP-LSzM3EEBdOTb9FaNA)

####  福利  ####

- [100个实战项目免费领取（非广告）](https://mp.weixin.qq.com/s/-MKinm7PLPxfcQwCJte6AQ)


##  关于JavaPub  ##


>**纯手打原创电子书**，**致力于最实用教程**，不需要什么奖励，只希望多多**转发支持**。

欢迎来到我公众号，微信搜索：**JavaPub**，回复：【**666**】即可领取

<a name="公众号"><img src="https://tva4.sinaimg.cn/mw690/007F3CC8ly1h0jpebzb51j3076076glw.jpg" alt="公众号"></a>



---

---



P哥微信：

<a name="P哥微信"><img src="https://tva2.sinaimg.cn/mw690/007F3CC8ly1h0jpmrt2fcj30by0byq3j.jpg" alt="P哥微信"></a>